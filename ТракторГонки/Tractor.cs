﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace ТракторГонки
{
    public partial class Tractor : UserControl
    {
        TractorDrow tracktor = new TractorDrow();
        public Tractor()
        {
            InitializeComponent();
        }

        private void Tractor_Load(object sender, EventArgs e)
        {

        }

        private void Tractor_Paint(object sender, PaintEventArgs e)
        {
            tracktor.Draw(e.Graphics);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ТракторГонки
{
    class TractorDrow
    {
        public Cabine cabine;
        public Body body;
        public Whell wheel;


        public TractorDrow()
        {
            cabine = new DefCabina();
            body = new DefBody();
            wheel = new DefWhell();
        }


        public void Draw(Graphics g)
        {
            cabine.Draw(g);
            body.Draw(g);
            wheel.Draw(g);

        }
    }
}

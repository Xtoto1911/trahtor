﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ТракторГонки
{
    class NormWheel: Whell
    {
        public NormWheel() { }
        public override void Draw(Graphics g)
        {
            g.DrawImage(Properties.Resources.bbs, x, y, d, d);
            g.DrawImage(Properties.Resources.bbs, x + 280, y - d * 2 / 2 / 4, d * 3 / 2, d * 3 / 2);
            this.x = x;
        }
    }
}

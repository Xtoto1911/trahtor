﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ТракторГонки
{
    abstract class Body
    {
        public int x = -100;
        public int y = 0;
        public int w = 1000;
        public int h = 500;
        public abstract void Draw(Graphics g);
    }
}

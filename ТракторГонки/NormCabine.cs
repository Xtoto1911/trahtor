﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace ТракторГонки
{
    class NormCabine: Cabine
    {
        public override void Draw(Graphics g)
        {
            g.DrawImage(Properties.Resources.coolerCabine, x, y, w, h);
            this.x = x;
        }
    }
}

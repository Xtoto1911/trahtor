﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ТракторГонки
{
    class DefWhell:Whell
    {
        public DefWhell() { }
        public override void Draw(Graphics g)
        {
            g.DrawImage(Properties.Resources.wheel, x, y, d, d);
            g.DrawImage(Properties.Resources.wheel, x + 280, y - d * 3 / 2 / 4, d * 3 / 2, d * 3 / 2);
            this.x = x;
        }
    }
}
